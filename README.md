# Requisitos

    *   IDE Arduino
    *   Librerías Blynk
    
# Conexión DHT22

Pin  | Port
------------- | -------------
1  | Vcc
2  | Data
3  | NA
4  | gnd
    
# Asociar a proyecto Blynk

Al crear un proyecto en la aplicación blynk, nos da un token. Este token se debe remplazar en el código arduino para poder conectar ambos extremos del sistema.

# Ejecución 

Ejecutar `blynk-ser.sh` en linux o `blynk-ser.bat` en windows con el puerto del arduino como parámetro.

`./blynk-ser.sh -c COM3`
