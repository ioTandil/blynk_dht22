#include <BlynkSimpleStream.h>
#include "DHT.h"

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
const char auth[] = "d46ec144c05e49b7ba219c99e9dfe5f2";

#define DHTPIN 2 // qué pin estamos conectados
#define DHTTYPE DHT22 // DHT 22 (AM2302)

DHT dht (DHTPIN, DHTTYPE);

BLYNK_READ(V0)
{
  float h = dht.readHumidity();
  Blynk.virtualWrite(V0, h);
}  

BLYNK_READ(V1)
{
  float t = dht.readTemperature();
  Blynk.virtualWrite(V1, t);
}  

void setup()
{
  Dht.begin();
  Serial.begin(9600);
  Blynk.begin(Serial, auth);
}

void loop()
{
  Blynk.run();
}
